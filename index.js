function findMin() {
  for (var num = 0, e = 0, total = 0; num < 10000; num++) {
    total += num;
    if (total > 10000) {
      e = num;
      break;
    }
  }
  document.getElementById(
    "txtResult1"
  ).innerHTML = `số nguyên dương n nhỏ nhất thỏa điều kiện :${e}`;
}
// bài 2
function calSum() {
  var x = +document.getElementById("inputX").value;
  var n = +document.getElementById("inputN").value;
  for (var e = 1, s = 0; e <= n; e++) {
    s += Math.pow(x, e);
  }
  document.getElementById("txtResult2").innerHTML = s;
}
// bài 3
function calGT() {
  var num = +document.getElementById("inputN1").value;

  for (var i = 1, n = 1; i <= num; i++) {
    n *= i;
  }
  document.getElementById(
    "txtResult3"
  ).innerHTML = `Giai thừa: ${n.toLocaleString()}`;
}
// bai 4
function makeDiv() {
  for (var e = "", t = 1; t <= 10; t++) {
    e +=
      t % 2 == 0
        ? "<div class='bg-danger text-white p-2'>Div chẵn</div>"
        : "<div class='bg-primary text-white  p-2'>Div lẻ </div>";
  }
  document.getElementById("txtResult4").innerHTML = e;
}
